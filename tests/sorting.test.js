const { test, expect } = require('@playwright/test');
const LoginPage = require('../pages/LoginPage');
const HomePage = require('../pages/HomePage');
const loginData = require('../data/loginData.json');

test.describe('Sorting Tests', () => {
    test.beforeEach(async ({ page }) => {
        const loginPage = new LoginPage(page);
        await loginPage.navigateTo();
        await loginPage.login();
    });

    test('should sort items by name A -> Z', async ({ page }) => {
        const homePage = new HomePage(page);
        await homePage.sortItemsByName('az');
        const isSorted = await homePage.isSorted('az');
        expect(isSorted).toBe(true);
        console.log(homePage.getItemNames);
    });
    test('should sort items by name Z -> A', async ({ page }) => {
        const homePage = new HomePage(page);
        await homePage.sortItemsByName('za');
        const isSorted = await homePage.isSorted('za');
        expect(isSorted).toBe(true);
        console.log(homePage.getItemNames);
    });

   
});
