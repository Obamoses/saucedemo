
module.exports = {
  usernameInput: '[data-test="username"]',
  passwordInput: '[data-test="password"]',
  loginButton: '[data-test="login-button"]'
};


module.exports = {
  productSortDropdown: '.product_sort_container',
  inventoryItems: '.inventory_item',
  inventoryItemName: '.inventory_item_name'
};