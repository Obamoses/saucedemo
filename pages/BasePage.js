
const config = require('../config');

class BasePage {
  constructor(page) {
    this.page = page;
    this.baseUrl = config.baseUrl;
  }

  async navigateTo(path = '') {
    await this.page.goto(this.baseUrl + path);
  }
}

module.exports = BasePage;