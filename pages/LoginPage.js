

const BasePage = require('./BasePage');
const loginData = require('../data/loginData.json');
const { expect } = require('@playwright/test');
class LoginPage extends BasePage {
  constructor(page) {
    super(page);
  }

    async login() {

      await this.page.locator('[data-test="username"]').fill(loginData.validUser.username);
      await this.page.locator('[data-test="password"]').click();
      await this.page.locator('[data-test="password"]').fill(loginData.validUser.password);
      await this.page.locator('[data-test="login-button"]').click();
     // await expect(this.page.getByText('Swag Labs')).toBeVisible();
    }
  }

module.exports = LoginPage;
