

const BasePage = require('./BasePage');
const locators = require('../locators/locators');

class HomePage extends BasePage {
  constructor(page) {
    super(page);
  }

  async sortItemsByName(option) {
    const sortBy = await this.page.locator('[data-test="product-sort-container"]');
    await sortBy.selectOption(option);

  }

  async getItemNames() {
    const itemElements = await this.page.$$(locators.inventoryItemName);
    return Promise.all(itemElements.map(item => item.innerText()));
  }

async getItems() {
  // Retrieve the list of item elements
  const items = await this.page.locator('#inventory_container');
  // Get the text content of all item elements and return as an array of strings
  const itemNames = await items.allTextContents();
  console.log(itemNames);
  return itemNames;
}
async isSorted(order = 'az') {
  const itemNames = await this.getItems();

  // Check if the array is sorted correctly
  for (let i = 0; i < itemNames.length - 1; i++) {
    if (order === 'az' && itemNames[i].localeCompare(itemNames[i + 1]) > 0) {
      return false;
    }
    if (order === 'za' && itemNames[i].localeCompare(itemNames[i + 1]) < 0) {
      return false;
    }
  }

  return true;
}
}

module.exports = HomePage;
