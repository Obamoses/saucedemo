FROM mcr.microsoft.com/playwright:v1.44.1-focal

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the project files
COPY . .

# Run tests
CMD ["npx", "playwright", "test"]